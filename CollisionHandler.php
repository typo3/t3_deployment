<?php

/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 5/3/16
 * Time: 6:15 PM
 */

ini_set('register_argc_argv','on');
if (PHP_SAPI != "cli") {

    $ini="./config.ini";
} else {
    if (isset($argv[1])) {
        $ini = $argv[1];
    } else {
        exit(1);
    }
}

/**
 * Class GlobalFunctions
 */
class GlobalFunctions {

    /**
     * @var bool
     */
    static $isFirstLine = true;
    /**
     * @var array
     */
    static public $renumberArray = array();
    /**
     * @var array
     */
    static public $finalRenumberArray = array();

    static $lightRed = "\033[0;31m";
    static $lightGreen = "\033[0;32m";
    static $lightBlue = "\033[0;34m";
    static $resetColor = "\033[0m";



    /**
     * @param $string
     */
    public static function returnAsTitle($string) {
        if (GlobalFunctions::$isFirstLine) {
            GlobalFunctions::$isFirstLine = false;
        } else {
            echo PHP_EOL;
        }
        echo '------------------------------------------------------------------' . PHP_EOL;
        echo ' ' . $string . PHP_EOL;
        echo '------------------------------------------------------------------' . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsList($string) {
        echo '  * ' . GlobalFunctions::$lightGreen . $string . GlobalFunctions::$resetColor . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsNormal($string) {
        echo PHP_EOL . ' ' . $string . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsError($string) {
        echo PHP_EOL . "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        echo "!! ERROR: " . GlobalFunctions::$lightRed . $string . GlobalFunctions::$resetColor . PHP_EOL;
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        exit(1);
    }

    public static function evaluateInput($string) {
        echo PHP_EOL . PHP_EOL . " " . GlobalFunctions::$lightBlue . $string . GlobalFunctions::$resetColor;
        $handle = fopen ("php://stdin","r");
        $continue = true;
        while ($continue) {
            $line = fgets($handle);
            switch(trim($line)) {
                case 'no';
                case 'No';
                case 'n':
                    return false;
                case 'Yes';
                case 'y';
                case 'yes':
                    return true;
                case 'Cancel';
                case 'cancel':
                    echo "Program cancelled by user";
                    exit(1);
            }
        }
        return false;
    }

}

/**
 * Class ParseConfig
 */

class ParseConfig
{

    /**
     * @var string
     */
    public $iniFile = null;


    /**
     * @param string $iniFilePath path to ini file
     */
    public function __construct($iniFilePath) {
        $this->iniFile = $iniFilePath;
    }

    /**
     * @param String $ini Path to Config ini
     * @return array
     * @throws Exception Will be thrown if config.ini can't be accessed.
     */
    function parse_ini($ini) {
        if (file_exists($ini)) {
            $parse = parse_ini_file($ini, true);
            return $parse;
        } else {
            throw new Exception("Config file can't be accessed. Aborting.");
        }
    }

    /**
     * @param String $string Name of the option
     * @param array $parse Handle for the ini file
     * @return mixed
     * @throws Exception Will be thrown if option can't be read.
     */
    function get_option($string, $parse) {
        if (!empty($parse)) {
            $output = $parse[$string];
        } else {
            throw new Exception("Config file can't be accessed.");
        }
        if (isset($output)) {
            return $output;
        } else {
            throw new Exception("Option $string not defined in config.ini.");
        }
    }

    /**
     * @return mixed Path to folder containing local copy of git branch
     */
    public function getPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("branch_path", $parse);
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }

    /**
     * @return mixed Path to folder containing local copy of git trunk
     */
    public function getTrunkPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("trunk_path", $parse);
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }

    /**
     * @return bool Checks config, returns true if form2 tables should be handled
     */
    public function hasForm() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_form2", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }

    /**
     * @return bool Checks config, returns true if qfq tables should be handled
     */
    public function hasQFQ() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_qfq", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }

    /**
     * @return string A temporary output path for the merged files generated by this script
     */
    public function getOutputPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("tmp_path", $parse);
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }

    /**
     * SQL Lite in memory database connection - no ini parsing needed.
     * @return PDO database connection handler
     */
    public function getConnection() {
        try {
            $link = new PDO('sqlite::memory:');
            $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $link;
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError("Connection failed: " . $e->getMessage());
        }

        return false;
    }

    /**
     * @param bool $getForm if true, returns the database containing the form2/qfq tables, else returns t3 database
     * @return PDO link to MySQL Database defined in config.ini
     */
    public function getDevelopmentDatabaseConnection($getForm) {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $driver = $this->get_option("db_driver", $parse);
            $dsn = "${driver}:";
            $user = $this->get_option("db_user", $parse);
            $password = $this->get_option("db_password", $parse);
        }
        catch (Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }

        if (!empty($parse)) {
            if (!$getForm) {

                foreach ($parse ["branch_t3"] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            } else {
                foreach ($parse ["branch_db"] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            }
        }
        try {
            if (!empty($dsn) && !empty($user) && !empty($password)) {
                $link = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $link;
            }
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError("Connection failed: " . $e->getMessage());
            exit(1);
        }
        return false;
    }
}

/**
 * Class ResultSet
 */
class ResultSet {

    /**
     * @var array
     */
    public $objectList;
    /**
     * @var string
     */
    public $table;
    /**
     * @var string
     */
    public $primaryId;

    /**
     * @param string $table name of the table this resultset handles
     */
    function __construct($table) {
        $this->objectList = array();
        $this->table = $table;
        if (fnmatch("form*", $table, FNM_CASEFOLD)) {
            $this->primaryId = 'id';
        } else {
            if (fnmatch("Form*", $table)) {
                $this->primaryId = 'id';
            } else {
                $this->primaryId = 'uid';
            }
        }
    }

    /**
     * @param $id
     * @return Readout
     */
    public function returnRow($id) {
        return $this->objectList[$id];
    }

    public function returnLength() {
        return sizeof($this->objectList);
    }

    public function doesExist($id) {
        if ($this->objectList[$id]) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * Class Readout
 */
class Readout {
    /**
     * @var string Row array formatted in JSON
     */
    public $data_json;
    /**
     * @var string Path to file referenced
     */
    public $path;
    /**
     * @var string Folder containing the file
     */
    public $folder;
    /**
     * @var string Original table name
     */
    public $myTable;
    /**
     * @var array Contains an array with one entry of the table
     */
    public $data_array;
    /**
     * @var int Unique ID (in the table)
     */
    public $uid;
    /**
     * @var ParseConfig
     */
    public $parseConfig;
    /**
     * @var string Name of the primary key.
     */
    public $primaryId;
    /**
     * @var bool
     */
    public $isForm;

    function readFile() {
        $handler = fopen($this->path, 'r') or die ("Unable to open file!");
        $this->data_json = fread($handler, filesize($this->path));
        fclose($handler);
    }

    function decodeJSON() {
        $this->data_array = json_decode($this->data_json, true);
    }

    /**
     * @param ParseConfig $parseConfig
     * @param string $folder
     * @param string $class
     * @param ResultSet $resultset
     * @param bool $isTrunk
     */
    static public function readDirectory($parseConfig, $folder, $class, $resultset, $isTrunk) {
        if ($isTrunk) {
            $path = $parseConfig->getTrunkPath();
        } else {
            $path = $parseConfig->getPath();
        }
        $path= $path . $folder;
        GlobalFunctions::returnAsList("Importing from " . $path);
        if($dir = opendir($path)) {
            while (($file = readdir($dir)) !== false) {
                if (!in_array($file, array('.', '..')) && !is_dir($path.$file)) {
                    /** @var Readout $currentRow */
                    $currentRow = new $class;
                    $currentRow->uid = $file;
                    $currentRow->folder = $folder;
                    $currentRow->path = $path . '/' . $currentRow->uid;
                    $currentRow->parseConfig = $parseConfig;
                    $currentRow->readFile();
                    $currentRow->decodeJSON();
                    $resultset->objectList[$file]=$currentRow;

                }
            }
            closedir($dir);
        }
    }

    /**
     * @param ParseConfig $parseConfig
     * @param string $table
     * @param string $class
     * @param ResultSet $resultSet
     */
    static function readFromMySQL($parseConfig, $table, $class, $resultSet) {
        if (fnmatch("form*", $table, FNM_CASEFOLD)) {
            $isForm = true;
            $primaryId = "id";
        } else {
            $isForm = false;
            $primaryId = "uid";
        }
        GlobalFunctions::returnAsList("Importing data from table " . $table);
        $path = $parseConfig->getPath();
        try {
            $link = $parseConfig->getDevelopmentDatabaseConnection($isForm);
            /** @var PDOStatement $stmt */
            $stmt = $link->prepare("SELECT * FROM " . $table . ";");
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $curPage = new $class;
                $uid = $row[$primaryId];
                $curPage->data_array = $row;
                $curPage->path = $path . $curPage->folder;
                $curPage->uid = $uid;
                $curPage->parseConfig = $parseConfig;
                $resultSet->objectList[$uid] = $curPage;
            }
            $stmt -> closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

}

/**
 * Class Pages
 */
class Pages extends Readout {

    function __construct() {
        $this->folder = 'pages';
        $this->myTable = $this->folder;
        $this->isForm = false;
        $this->primaryId = "uid";
    }
}

/**
 * Class TtContent
 */
class TtContent extends Readout {

    function __construct() {
        $this->folder = 'tt_content';
        $this->myTable = $this->folder;
        $this->isForm = false;
        $this->primaryId = "uid";
    }
}

/**
 * Class Form
 */
class Form extends Readout {

    function __construct() {
        $this->folder = 'form';
        $this->myTable = $this->folder;
        $this->isForm = true;
        $this->primaryId = "id";
    }
}

/**
 * Class FormElement
 */
class FormElement extends Readout {

    function __construct() {
        $this->folder = 'form_element';
        $this->myTable = $this->folder;
        $this->isForm = true;
        $this->primaryId = "id";
    }
}

/**
 * Class QFQForm
 */
class QFQForm extends Readout {

    function __construct() {
        $this->folder = 'Form';
        $this->myTable = $this->folder;
        $this->isForm = true;
        $this->primaryId = "id";
    }
}

/**
 * Class QFQFormElement
 */
class QFQFormElement extends Readout {

    function __construct() {
        $this->folder = 'FormElement';
        $this->myTable = $this->folder;
        $this->isForm = true;
        $this->primaryId = "id";
    }
}

/**
 * Class CreateTable
 */
class CreateTable {
    /**
     * @var string
     */
    public $table;
    /**
     * @var ResultSet
     */
    public $resultset;
    /**
     * @var string
     */
    public $primaryId;
    /**
     * @var PDO
     */
    public $link;
    /**
     * @var bool
     */
    public $usingResultset;
    /**
     * @var array
     */
    public $tablesCreated;

    /**
     * @param ParseConfig $parseConfig
     * @param ResultSet $resultset
     * @param string $class
     * @param string $tableSuffix
     * @param PDO $link
     */
    function __construct($parseConfig, $resultset, $class, $tableSuffix, $link) {
        $this->resultset = $resultset;
        $tempObj = new $class;
        $this->table = $tempObj->myTable . $tableSuffix;
        $this->primaryId = $tempObj->primaryId;
        $this->link=$link;
        $this->tablesCreated = array();
        if ($tableSuffix == "d") {
            $this->usingResultset = false;
        } else {
            $this->usingResultset = true;
        }
    }

    /**
     * @param string $string
     * @return string
     */
    function escapeString($string) {
        # Escaping single ticks, because the Values will be wrapped in single ticks.
        $string = str_replace("'", "''",$string);
        # Wrapping Value in single Ticks
        return "'". $string ."'";
    }

    /**
     * @param integer $id
     */
    function insertRow($id) {
        $thisResultset = $this->resultset;
        $currentRow = $thisResultset->returnRow($id);
        $columns = implode(", ",array_keys($currentRow->data_array));
        $escaped_values = array_map(array(&$this, "escapeString"), array_values($currentRow->data_array));
        $values  = implode(", ", $escaped_values);
        try {
            $link = $this->link;
            /** @var PDOStatement $sql */
            $sql = $link->prepare("INSERT INTO `" . $this->table . "`($columns) VALUES ($values);");
            $sql->execute();
            $sql->closeCursor();
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

    /**
     * @param string $columns
     * @return string
     * @throws Exception
     */
    function setUniqueIndex($columns) {
        $haystack = $columns;
        $needle = $this->primaryId;
        $replace = $this->primaryId. " INTEGER PRIMARY KEY";
        $pos = strpos($haystack, $needle);
        if ($pos !== false) {
            $newstring = substr_replace($haystack, $replace, $pos, strlen($needle));
            return $newstring;
        } else {
            throw new Exception("Couldn't set unique index. Aborting");
        }
    }

    /**
     * Creates the table using one object of each table as reference
     */
    function createTable() {
        $thisResultset = $this->resultset;
        if (!empty(array_values($thisResultset->objectList)[0])) {
            $currentRow = array_values($thisResultset->objectList)[0];
        } else {
            GlobalFunctions::returnAsError("Changeset for Table " . $thisResultset->table . " is empty");
        }
        try {
            if (!empty($currentRow)) {
                $columns = implode(", ", array_keys($currentRow->data_array));
            } else {
                throw new Exception("Couldn't access dataset to create a database");
            }
            $columns = $this->setUniqueIndex($columns);
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        try {
            $link = $this->link;
            /** @var PDOStatement $sql */
            if (!empty($columns)) {
                $sql = $link->prepare("CREATE TABLE `" . $this->table . "`($columns);");
            } else {
                throw new Exception("Couldn't read columns");
            }
            $sql->execute();
            $sql->closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

    /**
     * Inserts all the rows of current Resultset into a sqlite table
     */
    public function updateDatabase() {
        $resultset = $this->resultset;
        $results = $resultset->objectList;
        $this->createTable();
        foreach($results as $row) {
            $this->insertRow($row->uid);
        }
    }

}

/**
 * Class DatasetImport
 */
Class DatasetImport {
    /**
     * @var array
     */
    public $tablesCreated;
    /**
     * @var PDO
     */
    public $link;
    /**
     * @var string
     */
    public $tableSufix;
    /**
     * @var array
     */
    public $alreadyInserted;
    /**
     * @var array
     */
    public $maybeRenumber;

    /**
     * @param PDO $link Constructor requires a PDO Link
     */
    public function __construct($link) {
        $this->link = $link;
        $this->tableSufix = "d";
        $this->tablesCreated = array();
        $this->alreadyInserted = array();
        $this->maybeRenumber = array();
    }

    /**
     * @param string $columns
     * @param string $primaryId
     * @return mixed string $newstring or error
     * @throws Exception
     */
    function setUniqueIndex($columns, $primaryId) {
        $haystack = $columns;
        $needle = $primaryId;
        $replace = $primaryId. " INTEGER PRIMARY KEY";
        $pos = strpos($haystack, $needle);
        if ($pos !== false) {
            $newstring = substr_replace($haystack, $replace, $pos, strlen($needle));
            return $newstring;
        } else {
            throw new Exception("Couldn't set unique index. Aborting.");
        }
    }

    /**
     * @param TestResult $result
     */
    function createTableFromResult($result) {
        try {
            $columns = implode(", ", array_keys($result->row));
            $columns = $this->setUniqueIndex($columns, $result->primaryId);
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        $table = $result->tableOut . $this->tableSufix;
        try {
            $link = $this->link;
            if (!empty($columns)) {
                $sql = $link->prepare("CREATE TABLE `" . $table . "`($columns);");
            } else {
                throw new Exception("Variable columns not defined.");
            }
            $sql->execute();
            $sql->closeCursor();
        }
        catch(Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
        $this->tablesCreated[] = $result->tableOut;
    }

    /**
     * @param string $string
     * @return string
     */
    function escapeString($string) {
        # Escaping single ticks, because the Values will be wrapped in single ticks.
        $string = str_replace("'", "''",$string);
        # Wrapping Value in single Ticks
        return "'". $string ."'";
    }

    /**
     * @param TestResult $result
     */
    function insertEntry($result) {

            $columns = implode(", ",array_keys($result->row));
            $escaped_values = array_map(array(&$this, "escapeString"), array_values($result->row));
            $values  = implode(", ", $escaped_values);
            $table = $result->tableOut . $this->tableSufix;
            try {
                $link = $this->link;
                $sql = $link->prepare("INSERT INTO `" . $table . "`($columns) VALUES ($values);");
                $sql->execute();
                $sql->closeCursor();
            }
            catch(PDOException $e) {
                GlobalFunctions::returnAsError($result->table . " " . $result->primaryId . " " . $result->uid . $e->getMessage());
            }

    }

    /**
     * @param array $referenceArray
     */
    public function createTables($referenceArray) {
        GlobalFunctions::returnAsNormal("Inserting Entries");
        $counting = 0;
        foreach ($referenceArray as $result) {
            if (!in_array($result->tableOut, $this->tablesCreated)) {
                $this->createTableFromResult($result);
            }
            $this->insertEntry($result);
            $counting++;
        }
        GlobalFunctions::returnAsList("Inserted all $counting unchanged Entries in to D");

    }

    /**
     * @param string $ofTable
     * @param string $primaryId
     * @return Readout last Result of a table
     * @throws Exception if Highest ID can't be determined, throw error.
     */
    function getHighestId($ofTable, $primaryId) {
        try {
            /** @var PDOStatement $sql */
            $sql = $this->link->prepare("SELECT MAX($primaryId) FROM $ofTable");
            $sql->execute();
            $result = $sql->fetch();
            $sql->closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
        if (!empty($result[0])) {
            return $result[0];
        } else {
            throw new Exception("Couldn't determine highest ID of " . $ofTable);
        }
    }

}

/**
 * Class ExportFiles
 */
class ExportFiles {
    /**
     * @var array
     */
    public $tableInformations;

    /**
     * Constructing the big array
     * @param ParseConfig $parseConfig
     */
    public function __construct($parseConfig) {
        $pages = array('table'=>"pages", 'primaryId'=>"uid");
        $ttc = array('table'=>"tt_content", 'primaryId'=>"uid");
        $form = array('table' => "form", 'primaryId' => "id");
        $forme = array('table' => "form_element", 'primaryId' => "id");
        $qfqform = array('table'=>"Form", 'primaryId'=>"id");
        $qfqforme = array('table'=>"FormElement", 'primaryId'=>"id");
        $this->tableInformations=array($pages,$ttc);
        if ($parseConfig->hasForm()) {
            array_push($this->tableInformations, $form, $forme);
        }
        if ($parseConfig->hasQFQ()) {
            array_push($this->tableInformations, $qfqform, $qfqforme);
        }
    }

    /**
     * @param array $arrayToEncode
     * @return string
     */
    function encodeJson($arrayToEncode) {
        return json_encode($arrayToEncode, JSON_PRETTY_PRINT);
    }

    /**
     * @param ParseConfig $parseConfig
     * @param string $folder
     * @param integer $uid
     * @return string
     */
    function returnPath($parseConfig, $folder, $uid) {
        $path = $parseConfig->getOutputPath() . $folder . '/' . $uid;
        return $path;
    }

    /**
     * @param string $folder
     * @param ParseConfig $parseConfig
     */
    function createPath($folder, $parseConfig) {
        $mkDir = $parseConfig->getOutputPath() . $folder;
        if (!file_exists($mkDir)) {
            GlobalFunctions::returnAsList("Path: " . $mkDir . " will be created");
            mkdir($mkDir);
        } else {
            GlobalFunctions::returnAsError("Variable Folder not set for export.");
        }
    }

    /**
     * @param ParseConfig $parseConfig
     * @param string $folder
     * @param integer $uid
     * @param array $data_array
     * @param string $pathOri
     */
    function createFile($parseConfig, $folder, $uid, $data_array, $pathOri) {
        $path = $this->returnPath($parseConfig, $folder, $uid);
        if (!file_exists($pathOri)) {
            $this->createPath($folder, $parseConfig);
        }
        $handle = fopen($path, 'w');
        $json = $this->encodeJson($data_array);
        fwrite($handle, $json);
        fclose($handle);
    }

    /**
     * @param PDO $link
     * @param ParseConfig $parseConfig
     */
    public function exportData($link, $parseConfig) {
        $outputPath = $parseConfig->getOutputPath();
        GlobalFunctions::returnAsNormal("Exporting merged files");
        foreach($this->tableInformations as $tinfo) {
            $counting = 0;
            $primaryId = $tinfo['primaryId'];
            $sourceTable = $tinfo['table']."d";
            $folder = $tinfo['table'];
            try {
                $path = $outputPath . $folder;
                /** @var PDOStatement $stmt */
                $stmt = $link->prepare("SELECT * FROM " . $sourceTable . ";");
                $stmt->execute();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    $uid = $row[$primaryId];
                    $data_array = $row;
                    $this->createFile($parseConfig, $folder, $uid, $data_array, $path);
                    $counting++;
                }
                $stmt->closeCursor();
            }
            catch(PDOException $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
            GlobalFunctions::returnAsList("Created $counting Files for $folder");
        }
    }
}

/**
 * Class testResult
 * beefed up version of Readout results
 */
Class TestResult {
    /**
     * @var string
     */
    public $table;
    /**
     * @var integer
     */
    public $uid;
    /**
     * @var string
     */
    public $primaryId;
    /**
     * @var string
     */
    public $state;
    /**
     * @var array
     */
    public $row;
    /**
     * @var string
     */
    public $tableSufix;
    /**
     * @var string
     */
    public $tableOut;
    /**
     * @var string
     */
    public $referenceId;
    /**
     * @var bool
     */
    public $hasChildren = false;
    /**
     * @var bool Same table, but higher in the Hierarchy. Examples: FieldContainer, Subpages, etc.
     */
    public $hasOlderSibling = false;
    /**
     * @var integer
     */
    public $olderSibling;
    /**
     * @var array
     */
    public $myYoungerSiblings = [];
    /**
     * @var array
     */
    public $myChildren;
    /**
     * @var string
     */
    public $parentTable;
    /**
     * @var integer
     */
    public $rid;
    /**
     * @var integer
     */
    public $oldUid;
    /**
     * @var bool
     */
    public $hasSubpages = false;
    /**
     * @var string
     */
    public $identifier;
    /**
     * @var bool
     */
    public $wasRenumbered = false;

    /**
     * @param string $table
     * @param integer $uid
     * @param string $state
     * @param array $row
     */
    public function __construct($table, $uid, $state, $row) {
        $this->table = $table;
        $this->uid = $uid;
        $this->state=$state;
        $this->row = $row;
        $this->tableOut = substr($table,0,strlen($table)-1);
        $this->tableSufix = substr($table,-1,1);
        $this->parentTable = "";
        if (fnmatch("form*", $table, FNM_CASEFOLD)) {
            $this->primaryId = 'id';
            $this->referenceId = 'id';
            if ($this->tableOut == "form" || $this->tableOut == "Form") {
                $this->hasChildren = true;
            } else {
                if ($this->tableOut == "FormElement") {
                    $this->parentTable = "Form";
                    $this->referenceId = "formId";
                    if ($row["feIdContainer"] > 0) {
                        $this->hasOlderSibling = true;
                        $this->olderSibling = $row["feIdContainer"];
                    }
                } else {
                    $this->parentTable = "form";
                    $this->referenceId = 'form_id';
                }
            }
        } else {
            $this->primaryId = 'uid';
            $this->referenceId = 'pid';
            if ($this->tableOut == "pages") {
                $this->hasChildren = true;
                $this->myChildren = array();
                $this->parentTable = "pages";
            } else {
                $this->parentTable = "pages";
            }
        }
        $this->rid = $this->row["$this->referenceId"];
        $this->identifier = $this->tableOut . $this->uid . $this->state . $this->rid . "";
    }

    /**
     * @param string $table
     * @param integer $uid
     * @return bool
     */
    public function checkIfUidExists($table, $uid) {
        $tableOut = substr($table,0,strlen($table)-1);
        if ($this->tableOut == $tableOut && $this->uid == $uid) {
            return true;
        }
        return false;
    }

    /**
     * Setting new reference ID
     * @param integer $newId
     */
    function setNewRid($newId) {
        $this->rid = $newId;
        $this->row[$this->referenceId] = $newId;
        unset($this->row[$this->primaryId]);
        GlobalFunctions::returnAsList("Renumbered Child $this->referenceId to $newId. Table $this->tableOut");
    }

    /**
     * @param integer $newId
     */
    function setNewSubPageRid($newId) {
        $this->rid = $newId;
        $this->row["pid"] = $newId;
        GlobalFunctions::returnAsList("Renumbered Subpage $this->referenceId to $newId. Table $this->tableOut");
    }

    /**
     * @param integer $newId
     */
    function setNewSiblingId($newId) {
        $this->olderSibling = $newId;
        $this->row["feIdContainer"] = $newId;
        GlobalFunctions::returnAsList("Renumbered FormElement Container $this->olderSibling to $newId. Table $this->tableOut");
    }

    /**
     * @param string $demandedState
     * @return bool
     */
    public function checkState($demandedState) {
        if ($this->state == $demandedState) {
            return true;
        }
        return false;
    }

    /**
     * Set new ID
     * @param integer $newId
     */
    function setNewUid($newId) {
        $this->oldUid = $this->uid;
        $this->uid = $newId;
        $this->wasRenumbered = true;
        $this->row[$this->primaryId] = $newId;
        GlobalFunctions::returnAsList("Renumbered $this->primaryId $this->oldUid to $newId. Table $this->tableOut");
    }

    /**
     * @param array $arrayWithPrevious
     * @return bool
     */
    function alreadyUsed($arrayWithPrevious) {
        foreach ($arrayWithPrevious as $prev) {
            if ($this->rid == $prev->rid && $this->tableSufix=="b" && $this->tableOut == $prev->tableOut && $prev->tableSufix == "c") {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $fromThisLot
     * @return array
     */
    function collectMyChildren($fromThisLot) {
        $returnArray = array();
        foreach($fromThisLot as $maybeChild) {
            if (!$maybeChild->hasChildren) {
                if($this->uid == $maybeChild->rid && $this->tableOut == $maybeChild->parentTable AND $maybeChild->tableSufix == "b") {
                    $returnArray[] = $maybeChild;
                }
            }
        }
        return $returnArray;
    }

    /**
     * @param array $fromThisLot
     * @return array
     */
    function collectMyYoungerSiblings($fromThisLot) {
        $returnArray = array();
        foreach($fromThisLot as $maybeRelated) {
            if ($maybeRelated->hasOlderSibling) {
                if($this->uid == $maybeRelated->olderSibling && $this->tableOut == $maybeRelated->tableOut AND $maybeRelated->tableSufix == "b") {
                    $returnArray[] = $maybeRelated;
                }
            }
        }
        return $returnArray;
    }

    /**
     * @param array $fromThisArray
     * @return array
     */
    function removeMe($fromThisArray) {
        foreach($fromThisArray as $key=>$object) {
            if ($this->identifier == $object->identifier) {
                array_splice($fromThisArray,$key,1);
            }
        }
        return $fromThisArray;
    }

    /**
     * @param array $notInThisArray
     */
    function shouldntBeInThere($notInThisArray) {
        foreach($notInThisArray as $notMe) {
            if ($this->identifier ==  $notMe->identifier && $notMe->State != "4") {
                GlobalFunctions::returnAsError("Deleted Entry $this->uid is in Changeset - Faulty Datasets!");
            }
        }
    }

    /**
     * @param array $resultsArray
     * @return bool
     */
    function isParentInArray($resultsArray) {
        foreach($resultsArray as $possiblyParent) {
            if ($possiblyParent->hasChildren) {
                if($this->rid == $possiblyParent->uid && $this->parentTable == $possiblyParent->tableOut) {
                    return true;
                }
            }
        }
        return false;
    }

}

Class GenerateDeleteScript {
    public $deletedEntries;

    public function __construct($deletedEntries, $changedEntries) {
        $cleanArray = array();
        foreach($deletedEntries as $maybeDelete) {
            $addMe = true;
            foreach($changedEntries as $doNotDelete) {
                if($maybeDelete->tableOut == $doNotDelete->tableOut && $maybeDelete->uid == $doNotDelete->uid) {
                    $addMe = false;
                    break;
                }
            }
            if($addMe) {
                $cleanArray[] = $maybeDelete;
            }
        }
        $this->deletedEntries = $cleanArray;
    }

    public function writeScript($parseConfig) {
        $fileContent = "";
        foreach($this->deletedEntries as $entry) {
            $fileContent = $fileContent . "git rm " . $entry->tableOut . "/" . $entry->uid . PHP_EOL;
        }
        $file = $parseConfig->getOutputPath() . "/.deleteScript";
        try {
            $handle = fopen($file, 'w');
            fwrite($handle, $fileContent);
            fclose($handle);
        }
        catch(Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }
}

/**
 * Class GenerateTables
 */
Class GenerateTables {
    /**
     * @var string
     */
    public $tableSufix;
    /**
     * @var ResultSet
     */
    public $pagesResultSet;
    /**
     * @var ResultSet
     */
    public $ttcontentResultSet;
    /**
     * @var ResultSet
     */
    public $formResultSet;
    /**
     * @var ResultSet
     */
    public $formEleResultSet;
    /**
     * @var ResultSet
     */
    public $qfqFormResultSet;
    /**
     * @var ResultSet
     */
    public $qfqFormEleResultSet;
    /**
     * @var array
     */
    public $resultSets;
    /**
     * @var ParseConfig
     */
    public $iniFile;
    /**
     * @var bool
     */
    public $isTrunk;
    /**
     * @var PDO
     */
    public $link;
    /**
     * @var array
     */
    public $highestId;


    /**
     * @param string $tableSufix
     * @param ParseConfig $iniFile
     * @param PDO $link
     */
    public function __construct($tableSufix, $iniFile, $link) {
        $this->tableSufix = $tableSufix;
        $this->iniFile = $iniFile;
        $this->pagesResultSet = new ResultSet("pages");
        $this->ttcontentResultSet = new ResultSet("tt_content");
        $this->formEleResultSet = new ResultSet("form_element");
        $this->formResultSet = new ResultSet("form");
        $this->qfqFormResultSet = new ResultSet("Form");
        $this->qfqFormEleResultSet = new ResultSet("FormElement");
        $this->link = $link;
        $this->resultSets = array($this->pagesResultSet, $this->ttcontentResultSet);
        if ($this->iniFile->hasForm()) {
            array_push($this->resultSets, $this->formResultSet, $this->formEleResultSet);
        }
        if ($this->iniFile->hasQFQ()) {
            array_push($this->resultSets, $this->qfqFormResultSet, $this->qfqFormEleResultSet);
        }
        if ($tableSufix=="c") {
            $this->isTrunk = true;
        } else {
            $this->isTrunk = false;
        }
        $this->highestId = array("pages"=>1,"tt_content"=>1, "form"=>1, "form_element"=>1, "Form"=>1, "FormElement"=>1);
    }

    /**
     * @param string $table
     * @param string $class
     * @param bool $fromMySQL
     * @param ResultSet $resultSet
     */
    function importRawData($table, $class, $fromMySQL, $resultSet) {
        if ($fromMySQL) {
            Readout::readFromMySQL($this->iniFile, $table, $class, $resultSet);
        } else {
            Readout::readDirectory($this->iniFile,$table,$class,$resultSet,$this->isTrunk);
        }
    }

    /**
     * @param bool $fromMySQL
     */
    public function importTables($fromMySQL) {
        GlobalFunctions::returnAsNormal("Creating tables for $this->tableSufix");


        $this->importRawData('pages','Pages',$fromMySQL,$this->pagesResultSet);
        $pages = new CreateTable($this->iniFile, $this->pagesResultSet, 'Pages', $this->tableSufix, $this->link);
        $pages->updateDatabase();

        $this->importRawData('tt_content','TtContent',$fromMySQL,$this->ttcontentResultSet);
        $ttcontent = new CreateTable($this->iniFile, $this->ttcontentResultSet, 'TtContent', $this->tableSufix, $this->link);
        $ttcontent->updateDatabase();

        if($this->iniFile->hasForm()) {
            $this->importRawData('form', 'Form', $fromMySQL, $this->formResultSet);
            $form = new CreateTable($this->iniFile, $this->formResultSet, 'Form', $this->tableSufix, $this->link);
            $form->updateDatabase();

            $this->importRawData('form_element', 'FormElement', $fromMySQL, $this->formEleResultSet);
            $formelement = new CreateTable($this->iniFile, $this->formEleResultSet, 'FormElement', $this->tableSufix, $this->link);
            $formelement->updateDatabase();
        }

        if($this->iniFile->hasQFQ()) {
            $this->importRawData('Form', 'QFQForm', $fromMySQL, $this->qfqFormResultSet);
            $qfqForm = new CreateTable($this->iniFile, $this->qfqFormResultSet, 'QFQForm', $this->tableSufix, $this->link);
            $qfqForm->updateDatabase();

            $this->importRawData('FormElement', 'QFQFormElement', $fromMySQL, $this->qfqFormEleResultSet);
            $qfqFormelement = new CreateTable($this->iniFile, $this->qfqFormEleResultSet, 'QFQFormElement', $this->tableSufix, $this->link);
            $qfqFormelement->updateDatabase();
        }
    }

    /**
     * @param array $referenceArray
     * @param string $stateToImport
     * @return array filtered by state, ie new.
     */
    function filterByState($referenceArray, $stateToImport) {
        $stateArray = array();

        /** @var TestResult $result */
        foreach($referenceArray AS $result) {
            if($result->checkState($stateToImport)) {
                $stateArray[] = $result;
                $alreadyUsed[] = $result;
            }
        }

        return $stateArray;
    }

    /**
     * @param array $referenceArray
     */
    public function importFromDataset($referenceArray) {
        $unchangedEntries = $this->filterByState($referenceArray, "4");
        #$unchangedEntries = array();
        $newEntries = $this->filterByState($referenceArray, "2");
        $changedEntries = $this->filterByState($referenceArray, "1");
        $deletedEntries = $this->filterByState($referenceArray, "3");
        $renumberEntries = $this->filterByState($referenceArray, "9");
        $importArray = $this->combineChanges($unchangedEntries, $newEntries, $changedEntries, $deletedEntries);
        $deleteProcess = new GenerateDeleteScript($deletedEntries, $changedEntries);
        $deleteProcess->writeScript($this->iniFile);

        $insertIntoD = new DatasetImport($this->link);
        $insertIntoD->createTables($importArray);
        $renumberedEntries = $this->processingRenumber($renumberEntries);
        #$renumberedEntries = $insertIntoD->renumberEntries(GlobalFunctions::$finalRenumberArray);
        $insertIntoD->createTables($renumberedEntries);
    }

    /**
     * Exporting results into Files
     */
    public function exportIntoFiles() {
        $exportChanges = new ExportFiles($this->iniFile);
        $exportChanges->exportData($this->link, $this->iniFile);
    }

    /**
     * @param array $unchangedEntries
     * @param array $newEntries
     * @param array $changedEntries
     * @param array $deletedEntries
     * @return array
     */
    function combineChanges($unchangedEntries, $newEntries, $changedEntries, $deletedEntries) {
        $returnArray = array();
        /** @var TestResult $entry */
        foreach ($unchangedEntries as $entry) {
            if ($entry->uid > $this->highestId[$entry->tableOut]) {
                $this->highestId[$entry->tableOut] = $entry->uid;
            }
            $returnArray[] = $entry;
        }

        foreach ($deletedEntries as $entry) {
            if ($entry->uid > $this->highestId[$entry->tableOut]) {
                $this->highestId[$entry->tableOut] = $entry->uid;
            }
            $entry->shouldntBeInThere($returnArray);
        }

        foreach ($changedEntries as $entry) {
            if ($entry->uid > $this->highestId[$entry->tableOut]) {
                $this->highestId[$entry->tableOut] = $entry->uid;
            }
            $returnArray[] = $entry;
        }

        foreach ($newEntries as $entry) {
            $returnArray[] = $entry;
        }

        return $returnArray;
    }

    function processingRenumber($referenceArray) {
        $returnArray = array();
        $parents = array();
        $parentsRe = array();
        $children = array();
        $childrenRe = array();

        GlobalFunctions::returnAsNormal("Renumbering Entries");

        foreach ($referenceArray as $entry) {
            $entry->oldUid = $entry->uid;
            if ($entry->hasChildren) {
                $parents[] = $entry;
            } else {
                $children[] = $entry;
            }
        }

        var_dump($children);
        /** @var TestResult $entry */
        foreach ($parents as $entry) {
            $newId = $this->highestId[$entry->tableOut] + 1;
            if ($entry->uid < $newId) {
                $entry->setNewUid($newId);
                $this->highestId[$entry->tableOut] = $newId;
            }

            $parentsRe[] = $entry;
        }

        /** @var TestResult $child */
        foreach($children as $child) {
            $newId = $this->highestId[$entry->tableOut] + 1;
            if ($entry->uid < $newId) {
                $entry->setNewUid($newId);
                $this->highestId[$entry->tableOut] = $newId;
            }

            if (empty($parentsRe)) {
                $returnArray[] = $child;
            } else {
                foreach ($parentsRe as $entry) {
                    if ($child->rid == $entry->oldUid && $child->parentTable == $entry->tableOut) {
                        $child->setNewRid($entry->uid);
                        if ($child->hasOlderSibling) {
                            $childrenRe[] = $child;
                        } else {
                            $returnArray[] = $child;
                        }
                    }
                }
            }
        }

        var_dump($returnArray);

        // Update Subpages (special routine since it is in the same table)
        $pages = array();
        foreach($parentsRe as $entry) {
            if ($entry->tableOut == "pages") {
                $pages[] = $entry;
            } else {
                $returnArray[] = $entry;
            }
        }

        foreach($pages as $entry) {
            $insert = true;
            foreach($pages as $parent) {
                if ($entry->rid == $parent->oldUid) {
                    $entry->setNewSubPageRid($parent->uid);
                    $returnArray[] = $entry;
                    $insert=false;
                    break;
                }
            }
            if($insert) {
                $returnArray[] = $entry;
            }
        }

        // Update FormElement with CointainerID.
        $formElement = array();
        foreach($returnArray as $entry) {
            if ($entry->tableOut == "FormElement" && $entry->olderSibling == 0) {
                $formElement[] = $entry;
            }
        }

        foreach($childrenRe as $youngerSibling) {
            $insert = true;
            foreach($formElement as $olderSibling) {
                if ($youngerSibling->olderSibling == $olderSibling->oldUid) {
                    $youngerSibling->setNewSiblingId($olderSibling->uid);
                    $returnArray[] = $youngerSibling;
                    $insert=false;
                    break;
                }
            }
            if ($insert) {
                $returnArray[] = $youngerSibling;
            }
        }

        return $returnArray;
    }


    /**
     * Filter duplicates
     *
     * Filters duplicates by checking if uid already exists in table. Special treatment if
     * the duplicate entry is from tables ending in b.
     * @param array $referenceArray
     * @param string $table
     * @param integer $uid
     * @return bool
     */
    function checkBeforeInsert($referenceArray, $table, $uid) {
        $doInsert = true;
        /** @var TestResult $result */
        foreach($referenceArray as $result) {
            if ($result->checkIfUidExists($table, $uid) && $result->state < 3) {
                $doInsert = false;
                if (!fnmatch("*b", $table)) {
                    GlobalFunctions::returnAsList("Changes for uid $uid already detected in $table");
                }
                break;
            }
        }
        return $doInsert;
    }

    /**
     * @param GenerateTables $tablesToCompare
     * @param array $referenceArray
     * @return array
     */
    public function changedEntries($tablesToCompare, $referenceArray) {
        GlobalFunctions::returnAsNormal("Checking for changed Entries between Tables ". $this->tableSufix . " and Tables " .$tablesToCompare->tableSufix);
        /** @var ResultSet $base */
        foreach($this->resultSets as $base) {
            $table = $base->table. $this->tableSufix;
            $otherTable = $base->table . $tablesToCompare->tableSufix;
            $howToGetColumns = $base->objectList;
            $howToGetColumns2 = array_values($howToGetColumns)[0];
            $dataArray = $howToGetColumns2->data_array;
            $selectors = "";
            foreach($dataArray as $key=>$value) {
                $selectors = $selectors . "OR t1." . $key . " != t2." . $key . " ";
            }
            $sqlStatement = "SELECT t1.* FROM  " . $table .
                " t1 LEFT JOIN " . $otherTable . " t2 ON t2." .
                $base->primaryId . " = t1. " . $base->primaryId .
                " WHERE 1 = 2 " . $selectors . ";";
            try {
                $link = $this->link;
                /** @var PDOStatement $sql */
                $sql = $link->prepare($sqlStatement);
                $sql->execute();

                while ($row = $sql->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    $result = new TestResult($table, $row[$base->primaryId], "1", $row);
                    if ($this->checkBeforeInsert($referenceArray, $table, $row[$base->primaryId])) {
                        GlobalFunctions::returnAsList("Entry uid " . $row[$base->primaryId] . " in " . $table);
                        $referenceArray[] = $result;
                    } else {
                        $conflict = "Entry uid " . $row[$base->primaryId] . " in " . $table;
                        $returnValue = GlobalFunctions::evaluateInput("Conflicting changed entries. This entry has already been changed since last checkout: ". PHP_EOL ." $conflict" . PHP_EOL . "Do you want to overwrite changes?" . PHP_EOL ."Type yes for overwritting changes, no for discarding your changes or cancel to stop the programm." . PHP_EOL);
                        if ($returnValue) {
                            $referenceArray = $this->removeResult($row[$base->primaryId], $base->table, $referenceArray);
                            $referenceArray[] = $result;
                            GlobalFunctions::returnAsList("Entry replaces uid " . $row[$base->primaryId] . " in " . $base->table);
                        } else {
                            GlobalFunctions::returnAsList("Kept existing entry with uid " . $row[$base->primaryId] . " in " . $base->table);
                        }
                    }
                }
                $sql->closeCursor();
            }
            catch(PDOException $e){
                GlobalFunctions::returnAsError($e->getMessage());
            }
        }

        return $referenceArray;
    }

    /**
     * @param integer $uid
     * @param string $table
     * @param array $referenceArray
     * @return mixed
     */
    public function removeResult($uid, $table, $referenceArray) {
        foreach ($referenceArray as $key=>$result) {
            if ($result->uid == $uid && $result->tableOut == $table) {
                array_splice($referenceArray,$key,1);
            }
        }
        return $referenceArray;
    }

    /**
     * @param GenerateTables $tablesToCompare
     * @param array $referenceArray
     * @return array
     */
    public function newEntries($tablesToCompare, $referenceArray) {
        GlobalFunctions::returnAsNormal("Checking for new Entries between Tables ". $this->tableSufix . " and Tables " .$tablesToCompare->tableSufix);
        foreach($this->resultSets as $base) {
            $table = $base->table. $this->tableSufix;
            $otherTable = $base->table . $tablesToCompare->tableSufix;
            $sqlStatement = "SELECT t1.* FROM  " . $table .
                " t1 LEFT JOIN " . $otherTable . " t2 ON t2." .
                $base->primaryId . " = t1. " . $base->primaryId .
                " WHERE t2." . $base->primaryId . " IS NULL;";
            try {
                $link = $this->link;
                /** @var PDOStatement $sql */
                $sql = $link->prepare($sqlStatement);
                $sql->execute();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    GlobalFunctions::returnAsList("New Entry uid " . $row[$base->primaryId] . " in " . $table);
                    $referenceArray[] = new TestResult($table, $row[$base->primaryId], "2", $row);
                }
                $sql->closeCursor();
            }
            catch(PDOException $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
        }
        return $referenceArray;
    }

    /**
     * @param GenerateTables $tablesToCompare
     * @param array $referenceArray
     * @return array
     */
    public function renumberNewEntries($tablesToCompare, $referenceArray) {
        GlobalFunctions::returnAsNormal("Checking for new Entries between Tables ". $this->tableSufix . " and Tables " .$tablesToCompare->tableSufix);
        /** @var ResultSet $base */
        foreach($this->resultSets as $base) {
            $table = $base->table. $this->tableSufix;
            $otherTable = $base->table . $tablesToCompare->tableSufix;
            $sqlStatement = "SELECT t1.* FROM  " . $table .
                " t1 LEFT JOIN " . $otherTable . " t2 ON t2." .
                $base->primaryId . " = t1. " . $base->primaryId .
                " WHERE t2." . $base->primaryId . " IS NULL;";
            try {
                $link = $this->link;
                $sql = $link->prepare($sqlStatement);
                $sql->execute();
                while ($row = $sql->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    GlobalFunctions::returnAsList("New Entry uid " . $row[$base->primaryId] . " in " . $table);
                    $referenceArray[] = new TestResult($table, $row[$base->primaryId], "9", $row);
                }
                $sql->closeCursor();
            }
            catch(PDOException $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
        }
        return $referenceArray;
    }

    /**
     * @param GenerateTables $tablesToCompare
     * @param array $referenceArray
     * @return array
     */
    public function deletedEntries($tablesToCompare, $referenceArray) {
        GlobalFunctions::returnAsNormal("Checking for deleted Entries between Tables ". $this->tableSufix . " and Tables " .$tablesToCompare->tableSufix);
        foreach($this->resultSets as $base) {
            $table = $base->table. $this->tableSufix;
            $otherTable = $base->table . $tablesToCompare->tableSufix;
            $sqlStatement = "SELECT t1.* FROM  " . $otherTable .
                " t1 LEFT JOIN " . $table . " t2 ON t2." .
                $base->primaryId . " = t1. " . $base->primaryId .
                " WHERE t2." . $base->primaryId . " IS NULL;";
            try {
                $link = $this->link;
                $sql = $link->prepare($sqlStatement);
                $sql->execute();

                while ($row = $sql->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    GlobalFunctions::returnAsList("Deleted Entry uid " . $row[$base->primaryId] . " in " . $table);
                    $referenceArray[] = new TestResult($table, $row[$base->primaryId], "3", $row);
                }
                $sql->closeCursor();
            }
            catch(PDOException $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
        }
        return $referenceArray;
    }

    /**
     * @param GenerateTables $tablesToCompare
     * @param array $referenceArray
     * @return array
     */
    public function unChangedEntries($tablesToCompare, $referenceArray) {
        GlobalFunctions::returnAsNormal("Checking for unchanged Entries between Tables ". $this->tableSufix . " and Tables " .$tablesToCompare->tableSufix);
        foreach($this->resultSets as $base) {
            $table = $base->table . $this->tableSufix;
            $otherTable = $base->table . $tablesToCompare->tableSufix;
            $howToGetColumns = $base->objectList;
            $howToGetColumns2 = array_values($howToGetColumns)[0];
            $dataArray = $howToGetColumns2->data_array;
            $selectors = "";
            foreach($dataArray as $key=>$value) {
                $selectors = $selectors . "AND t1." . $key . " = t2." . $key . " ";
            }
            $sqlStatement = "SELECT t1.* FROM  " . $table .
                " t1 LEFT JOIN " . $otherTable . " t2 ON t2." .
                $base->primaryId . " = t1. " . $base->primaryId .
                " WHERE 1 = 1 " . $selectors . ";";
            try {
                $link = $this->link;
                $sql = $link->prepare($sqlStatement);
                $sql->execute();
                $counting = 0;
                while ($row = $sql->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    $counting++;
                    $referenceArray[] = new TestResult($table, $row[$base->primaryId], "4", $row);
                }
                $sql->closeCursor();
            } catch (PDOException $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
            if (!empty($counting)) {
                GlobalFunctions::returnAsList("Found " . $counting . " unchanged Entries in " . $table);
            }
        }
        return $referenceArray;
    }

}

# Program starts here
$parseConfig = new ParseConfig($ini);
$link = $parseConfig->getConnection();

# Importing Data to temporary SQLite3 Database
GlobalFunctions::returnAsTitle("Importing Data from Branch, Development Database and Trunk");
$tablesA = new GenerateTables('a',$parseConfig, $link);
$tablesA->importTables(false);

$tablesB = new GenerateTables('b',$parseConfig, $link);
$tablesB->importTables(true);

$tablesC = new GenerateTables('c',$parseConfig, $link);
$tablesC->importTables(false);

# Checking for changes
GlobalFunctions::returnAsTitle("Searching differences in Datasets.");
$changes = array();

$changes = $tablesC->unChangedEntries($tablesB, $changes);

$changes = $tablesC->deletedEntries($tablesA, $changes);
$changes = $tablesB->deletedEntries($tablesA, $changes);

$changes = $tablesC->changedEntries($tablesA, $changes);
$changes = $tablesB->changedEntries($tablesA, $changes);

$changes = $tablesC->newEntries($tablesA, $changes);
$changes = $tablesB->renumberNewEntries($tablesA, $changes);

# Merging Tables into new Tables D
GlobalFunctions::returnAsTitle("Merging Changes for export");
$tablesD = new GenerateTables('d', $parseConfig, $link);
$tablesD->importFromDataset($changes);
$tablesD->exportIntoFiles();

echo PHP_EOL;
exit(0);