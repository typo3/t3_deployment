<?php

/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 4/29/16
 * Time: 12:40 PM
 */

ini_set('register_argc_argv','on');

$trunk = false;

if (PHP_SAPI != "cli") {
    $ini="./config.ini";
} else {
    if (isset($argv[1])) {
        $ini = $argv[1];
    } else {
        exit(1);
    }
    if (isset($argv[2])) {
        if ($argv[2] == "trunk") {
            $trunk = true;
        }
    }
}


/**
 * Class GlobalFunctions
 */
class GlobalFunctions {

    /**
     * @var bool
     */
    static $isFirstLine = true;
    static $lightRed = "\033[0;31m";
    static $lightGreen = "\033[0;32m";
    static $lightBlue = "\033[0;34m";
    static $resetColor = "\033[0m";

    /**
     * @param $string
     */
    public static function returnAsTitle($string) {
        if (GlobalFunctions::$isFirstLine) {
            GlobalFunctions::$isFirstLine = false;
        } else {
            echo PHP_EOL;
        }
        echo '------------------------------------------------------------------' . PHP_EOL;
        echo ' ' . $string . PHP_EOL;
        echo '------------------------------------------------------------------' . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsList($string) {
        echo '  * ' . GlobalFunctions::$lightGreen . $string . GlobalFunctions::$resetColor . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsNormal($string) {
        echo PHP_EOL . ' ' . $string . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsError($string) {
        echo PHP_EOL . "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        echo "!! ERROR:" . GlobalFunctions::$lightRed . $string . GlobalFunctions::$resetColor . PHP_EOL;
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        exit(1);
    }

}

/**
 * Class ParseConfig
 */

class ParseConfig
{

    /**
     * @var string
     */
    public $iniFile = null;


    /**
     * @param string $iniFilePath path to ini file
     */
    public function __construct($iniFilePath) {
        $this->iniFile = $iniFilePath;
    }

    /**
     * @param String $ini Path to Config ini
     * @return array
     * @throws Exception Will be thrown if config.ini can't be accessed.
     */
    function parse_ini($ini) {
        if (file_exists($ini)) {
            $parse = parse_ini_file($ini, true);
            return $parse;
        } else {
            throw new Exception("Config file can't be accessed. Aborting.");
        }
    }

    /**
     * @param String $string Name of the option
     * @param array $parse Handle for the ini file
     * @return mixed
     * @throws Exception Will be thrown if option can't be read.
     */
    function get_option($string, $parse) {
        if (!empty($parse)) {
            $output = $parse[$string];
        } else {
            throw new Exception("Config file can't be accessed.");
        }
        if (isset($output)) {
            return $output;
        } else {
            throw new Exception("Option $string not defined in config.ini.");
        }
    }

    /**
     * @return mixed Path to folder containing local copy of git branch
     */
    public function getPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            if ($GLOBALS['trunk']) {
                $output = $this->get_option("trunk_path", $parse);
            } else {
                $output = $this->get_option("branch_path", $parse);
            }
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }

    /**
     * @return bool Checks config, returns true if form2 tables should be handled
     */
    public function hasForm() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_form2", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }

    /**
     * @return bool Checks config, returns true if qfq tables should be handled
     */
    public function hasQFQ() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_qfq", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }


    /**
     * @param bool $getForm if true, returns the database containing the form2/qfq tables, else returns t3 database
     * @return PDO link to MySQL Database defined in config.ini
     */
    public function getConnection($getForm) {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $driver = $this->get_option("db_driver", $parse);
            $dsn = "${driver}:";
            $user = $this->get_option("db_user", $parse);
            $password = $this->get_option("db_password", $parse);
        }
        catch (Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }

        $parseTypo = "branch_t3";
        $parseForm = "branch_db";
        if ($GLOBALS["trunk"]) {
            $parseTypo = "trunk_t3";
            $parseForm = "trunk_db";
        }
        if (!empty($parse)) {
            if (!$getForm) {

                foreach ($parse [$parseTypo] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            } else {
                foreach ($parse [$parseForm] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            }
        }
        try {
            if (!empty($dsn) && !empty($user) && !empty($password)) {
                $link = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $link;
            }
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError("Connection failed: " . $e->getMessage());
            exit(1);
        }
        return false;
    }
}

/**
 * Class ResultSet
 */
class ResultSet {
    /**
     * @var array
     */
    public $objectList;

    function __construct() {
        $this->objectList = array();
    }

    /**
     * @param Integer $id
     * @return Readout
     * @throws Exception
     */
    public function returnRow($id) {
        $returnObject = $this->objectList[$id];
        if (!empty($returnObject)) {
            return $returnObject;
        } else {
            throw new Exception("No object with id: $id found");
        }
    }

    public function returnLength() {
        return sizeof($this->objectList);
    }

    /**
     * @param Integer $id
     * @return bool
     */
    public function doesExist($id) {
        if (isset($this->objectList[$id])) {
            return true;
        }
        return false;
    }

}

/**
 * Class Readout
 */
class Readout {
    /**
     * @var String
     */
    public $data_json;
    /**
     * @var String
     */
    public $path;
    /**
     * @var String
     */
    public $folder;
    /**
     * @var array
     */
    public $data_array;
    /**
     * @var Integer
     */
    public $uid;
    /**
     * @var ParseConfig
     */
    public $parseConfig;

    /**
     * @throws Exception
     */
    function readFile() {
        try {
            $this->setPath();
            $handler = fopen($this->path, 'r') or die ("Unable to open file!");
        }
        catch(Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
        if (!empty($handler)) {
            $this->data_json = fread($handler, filesize($this->path));
            fclose($handler);
        } else {
            throw new Exception("Couldn't get Path");
        }
    }

    /**
     * @throws Exception
     */
    function setPath() {
        $tempPath = $this->parseConfig->getPath() . $this->folder . '/' . $this->uid;
        if (file_exists($tempPath)) {
            $this->path = $tempPath;
        } else {
            throw new Exception("File can't be accessed");
        }
    }

    function decodeJSON() {
        $json = json_decode($this->data_json, true);
        $this->data_array = $json;
    }

    /**
     * @param ParseConfig $parseConfig
     * @param String $folder
     * @param String $class
     * @param ResultSet $resultset
     */
    static public function readDirectory($parseConfig, $folder, $class, $resultset) {
        $path= $parseConfig->getPath() . $folder;
        try {
            if ($dir = opendir($path)) {
                while (($file = readdir($dir)) !== false) {
                    if (!in_array($file, array('.', '..')) && !is_dir($path . $file)) {
                        /**
                         * @var Readout $currentRow
                         */
                        $currentRow = new $class;
                        $currentRow->uid = $file;
                        $currentRow->folder = $folder;
                        $currentRow->parseConfig = $parseConfig;
                        try {
                            $currentRow->readFile();
                            $currentRow->decodeJSON();
                        }
                        catch(Exception $e) {
                            GlobalFunctions::returnAsError($e->getMessage());
                        }
                        $resultset->objectList[$file] = $currentRow;

                    }
                }
            }
        }
        catch(Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

}

/**
 * Class Pages
 */
class Pages extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'uid';
    /**
     * @var string
     */
    public $myTable = 'pages';
}

/**
 * Class TtContent
 */
class TtContent extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'uid';
    /**
     * @var string
     */
    public $myTable = 'tt_content';
}

/**
 * Class Form
 */
class Form extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'id';
    /**
     * @var string
     */
    public $myTable = 'form';
}

/**
 * Class FormElement
 */
class FormElement extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'id';
    /**
     * @var string
     */
    public $myTable = 'form_element';
}

/**
 * Class QFQForm
 */
class QFQForm extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'id';
    /**
     * @var string
     */
    public $myTable = 'Form';
}

/**
 * Class QFQFormElement
 */
class QFQFormElement extends Readout {

    /**
     * @var string
     */
    public $primaryId = 'id';
    /**
     * @var string
     */
    public $myTable = 'FormElement';
}

/**
 * Class SyncDatabase
 */
class SyncDatabase {
    /**
     * @var string
     */
    public $table;
    /**
     * @var ResultSet
     */
    public $resultset;
    /**
     * @var string
     */
    public $primaryId;
    /**
     * @var PDO
     */
    public $link;

    /**
     * @param ParseConfig $parseConfig
     * @param ResultSet $resultset
     * @param string $class
     * @param bool $isForm
     * @throws Exception
     */
    function __construct($parseConfig, $resultset, $class, $isForm) {
        $this->resultset = $resultset;
        $tempObj = new $class;
        if (isset($tempObj)) {
            $this->table = $tempObj->myTable;
            $this->primaryId = $tempObj->primaryId;
            $this->link = $parseConfig->getConnection($isForm);
        } else {
            throw new Exception("Couldn't open Table");
        }
    }

    function escapeString($string) {
        # Escaping Backslashes. Important, if for whatever reason there is a backslash at the end of a value, which could escape the tick
        $string = str_replace('\\', '\\\\', $string);
        # Escaping single ticks, because the Values will be wrapped in single ticks.
        $string = str_replace("'", "\'",$string);
        # Just to be safe.
        $string = str_replace(';','\;', $string);
        # Wrapping Value in single Ticks
        return "'". $string ."'";
    }

    function replaceRow($id) {
        $thisResultset = $this->resultset;
        $currentRow = $thisResultset->returnRow($id);
        if (!empty($currentRow)) {
            $columns = implode(", ", array_keys($currentRow->data_array));
        } else {
            throw new Exception("Dataset couldn't be loaded correctly");
        }
        if (!empty($columns)) {
            $escaped_values = array_map(array(&$this, "escapeString"), array_values($currentRow->data_array));
        } else {
            throw new Exception("Couldn't escape columns");
        }
        if (!empty($escaped_values)) {
            $values = implode(", ", $escaped_values);
        } else {
            throw new Excpetion("Couldn't convert array escaped_values to string values");
        }
        try {
            $link = $this->link;
            /** @var PDOStatement $sql */
            $sql = $link->prepare("REPLACE INTO `" . $this->table . "`($columns) VALUES ($values);");
            $sql->execute();
            $sql->closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

    function cleanTable() {
        try {
            $link = $this->link;
            /**
             * var PDOStatement $stmt
             */
            $stmt = $link->prepare("SELECT * FROM " . $this->table . ";");
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $currentId = $row[$this->primaryId];
                if (!empty($currentId)) {
                    if (!$this->resultset->doesExist($currentId)) {
                        GlobalFunctions::returnAsList("Deleting entry with " . $this->primaryId . ': ' . $currentId . ' from table ' . $this->table);
                        $delstmt = $link->prepare("DELETE FROM " . $this->table . " WHERE " . $this->primaryId . " = " . $currentId . ';');
                        $delstmt->execute();
                        $delstmt->closeCursor();
                    }
                }
            }
            $stmt->closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }

    public function updateDatabase() {
        $this->cleanTable();
        $resultset = $this->resultset;
        $results = $resultset->objectList;
        foreach($results as $row) {
            $this->replaceRow($row->uid);
        }
    }

}

// Execution Code starts here

// Set ini File containing settings
$parseConfig = new ParseConfig($ini);

GlobalFunctions::returnAsTitle("Importing MySQL from " . $parseConfig->getPath());

$pages = new ResultSet();
Readout::readDirectory($parseConfig, 'pages', 'Pages', $pages);
$ttcontent = new ResultSet();
Readout::readDirectory($parseConfig, 'tt_content', 'TtContent', $ttcontent);

$updatingDatabase = new SyncDatabase($parseConfig, $pages, 'Pages', false);
$updatingDatabase->updateDatabase();
unset($updatingDatabase);
$updatingDatabase = new SyncDatabase($parseConfig, $ttcontent, 'TtContent', false);
$updatingDatabase->updateDatabase();
unset($updatingDatabase);

if ($parseConfig->hasForm()) {
    $forms = new ResultSet();
    Readout::readDirectory($parseConfig, 'form', 'Form', $forms);
    $formElements = new ResultSet();
    Readout::readDirectory($parseConfig, 'form_element', 'FormElement', $formElements);

    $updatingDatabase = new SyncDatabase($parseConfig, $forms, 'Form', true);
    $updatingDatabase->updateDatabase();
    unset($updatingDatabase);
    $updatingDatabase = new SyncDatabase($parseConfig, $formElements, 'FormElement', true);
    $updatingDatabase->updateDatabase();
    unset($updatingDatabase);
}

if ($parseConfig->hasQFQ()) {
    $qfqForms = new ResultSet();
    Readout::readDirectory($parseConfig, 'Form', 'QFQForm', $qfqForms);
    $qfqFormElements = new ResultSet();
    Readout::readDirectory($parseConfig, 'FormElement', 'QFQFormElement', $qfqFormElements);

    $updatingDatabase = new SyncDatabase($parseConfig, $qfqForms, 'QFQForm', true);
    $updatingDatabase->updateDatabase();
    unset($updatingDatabase);
    $updatingDatabase = new SyncDatabase($parseConfig, $qfqFormElements, 'QFQFormElement', true);
    $updatingDatabase->updateDatabase();
    unset($updatingDatabase);
}

GlobalFunctions::returnAsNormal("Files Exported.");
GlobalFunctions::returnAsList("Pages: " . $pages->returnLength());
GlobalFunctions::returnAsList("tt_content: " . $ttcontent->returnLength());
if ($parseConfig->hasForm()) {
    GlobalFunctions::returnAsList("Forms: " . $forms->returnLength());
    GlobalFunctions::returnAsList("Form Elemente: " . $formElements->returnLength());
} else {
    GlobalFunctions::returnAsList("Form2: not selected in config.ini");
}
if ($parseConfig->hasQFQ()) {
    GlobalFunctions::returnAsList("QFQ Forms: " . $qfqForms->returnLength());
    GlobalFunctions::returnAsList("QFQ Form Elemente: " . $qfqFormElements->returnLength());
} else {
    GlobalFunctions::returnAsList("QFQ: not selected in config.ini");
}

exit(0);