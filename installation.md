Installation
------------

![Overview.svg](https://git.math.uzh.ch/typo3/t3_deployment/uploads/59775b957acf9e6df0c7fd23d4935f8a/Overview.svg)

It is assumed, that there exist already a production server with a typo3
Installation and the developer is the first person that works on this particular t3 instance.

If it isn't the first developer, **do NOT create a new repository**. Grant
the developer access to the repository for this particular t3 instance and
be check the existing branch names to avoid conflicts. Then skip down to startBranch.

 - Productive Server: production :seven:
 - Developer: Alice
 - New Development Server: develop_alice :three:
 - Access to a gitlab server :one:
 
t3_deploy normally should be installed on the client of the developer
or a server the developer has access to with his LDAP Account :two: and on
a server that has access to the production database, to synchronize
changes back :six:.

Since it is executed over a bash scripts, requires both php and git in
commandline, we assume linux as the operating system of the developer.

Requirements:

 - PHP 5.4 or higher
 - Following PHP Packages: php5-sqlite3 php5-cli php5-mysqli
 - git :fire:

### Procedure on the production Server
 - install the missing requirements. :six:
  ```
  #Example for ubuntu 14.04
  apt-get install php5-sqlite3 php5-cli git
  cd ~
  git clone https://git.math.uzh.ch/typo3/t3_deployment.git
  cd t3_deployment
  vi config_production.ini # create according to example
  php5enmod sqlite3
  ssh-keygen -t rsa -b 4096 -C "email@uzh.ch"
  less ~.ssh/id_rsa.pub # copy key to a git profile with access to the repository
  vi /etc/php5/cli/php.ini # argv suchen und aktivieren
  ```
 - Create a msyql user with *select*, *delete* and *insert* rights on the 
   t3 and qfq databases.:seven:
 - Create a folder for the git repository :six:
 - Create a new git repository and give access to the developer alice :one:
 - Create a config file, you can leave the branch parts blank :six:
 - Run database_sync.sh -o initialize -c /etc/t3_deploy/config_production.ini -m "initial commit"  :six:
 
### config_production.ini :six:

```
branch_path=files/branch/
trunk_path=files/trunk/
tmp_path=files/new/
db_driver=mysql
db_user=deploy
db_password=topSecretPassword
git_repository=git@systemvcs.math.uzh.ch:web/production.git
git_branch=

export_form2=0
export_qfq=1

[branch_t3]
host=localhost
port=3306
dbname=

[branch_db]
host=localhost
port=3306
dbname=

[trunk_t3]
host=production
port=3306
dbname=t3_production

[trunk_db]
host=production
port=3306
dbname=qfq_production

```

### Procedure on the development Server
 - Install typo3 and mysql Server :three:
 - Import the current production databases :three:
 - Create users for typo3 and t3_deploy on your mysql server :three:
 - Copy the complete folder of the typo3 website over :three:
 - assuming path to webspace is /var/www/html and the website is named develop_alice :three:
 - update /var/www/html/develop_alice/typo3conf/LocalConfiguration with local db :three:
 - update /var/www/html/develop_alice/typo3conf/ext/qfq/config.ini with local db :three:
 - install all the requirements needed for t3_deploy :two:
     ```
     #Example for ubuntu 14.04
     apt-get install php5-sqlite3 php5-cli git
     php5enmod sqlite3
     cd /home/alice
     git clone https://git.math.uzh.ch/typo3/t3_deployment.git
     cd t3_deployment
     vi config_alice.ini # create according to example
     less /home/alice/.ssh/id_rsa.pub # key must be added to a profile on git with access to the repository (if not already existing)
     vi /etc/php5/cli/php.ini # argv suchen und aktivieren
     ```
 - create folder for git repository :two:
  - create subfolder trunk
  - create subfolder branch
  - create subfolder tmp
 - create config, development instance doesn't need access to trunk mysql database :two:
 - run database_sync.sh startBranch /home/alice/t3_deploy/config_alice.ini "initializing branch dev_alice" :two:
 
### config_alice.ini :two:

```
branch_path=/home/alice/webprojects/production/branch/
trunk_path=/home/alice/webprojects/production/trunk/
tmp_path=/home/alice/webprojects/production/new/
db_driver=mysql
db_user=developer
db_password=topSecretPassword
git_repository=git@systemvcs.math.uzh.ch:web/production.git
git_branch=dev_alice

export_form2=0
export_qfq=1

[branch_t3]
host=dev_alice
port=3306
dbname=t3_develop

[branch_db]
host=dev_alice
port=3306
dbname=qfq_develop

[trunk_t3]
host=localhost
port=3306
dbname=

[trunk_db]
host=localhost
port=3306
dbname=

``` 
 
### Test the setup
 - Login to the t3 backend of develop_alice :two:
 - Create a new page :two:
 - Execute: database_sync.sh -o synchronize -c /home/alice/config_alice.ini -m "First test commit" :two:
 - Check back to develop if the new page is still there. :two:
 - Check in git repository, if the new page id shows up in the folder pages :one:
 - (optional) Execute database_sync.sh -o updateTrunk -c path_to_config -m "updating Trunk" on production :six:
 - Check if the new page appears on the production server :two: