<?php

/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 4/22/16
 * Time: 2:35 PM
 */

ini_set('register_argc_argv','on');
$trunk = false;

if (PHP_SAPI != "cli") {
    $ini = "./config.ini";
    if($_GET["trunk"]=="1") {
        $trunk = true;
    }
} else {
    if (isset($argv[1])) {
        $ini = $argv[1];
    } else {
        exit(1);
    }
    if (isset($argv[2])) {
        if ($argv[2] == "trunk") {
            $trunk = true;
        }
    }
}


/**
 * Class GlobalFunctions
 */
class GlobalFunctions {

    /**
     * @var bool
     */
    static $isFirstLine = true;
    /**
     * @var array
     */
    static public $renumberArray = array();
    /**
     * @var array
     */
    static public $finalRenumberArray = array();
    static $lightRed = "\033[0;31m";
    static $lightGreen = "\033[0;32m";
    static $lightBlue = "\033[0;34m";
    static $resetColor = "\033[0m";



    /**
     * @param $string
     */
    public static function returnAsTitle($string) {
        if (GlobalFunctions::$isFirstLine) {
            GlobalFunctions::$isFirstLine = false;
        } else {
            echo PHP_EOL;
        }
        echo '------------------------------------------------------------------' . PHP_EOL;
        echo ' ' . $string . PHP_EOL;
        echo '------------------------------------------------------------------' . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsList($string) {
        echo '  * ' . GlobalFunctions::$lightGreen . $string . GlobalFunctions::$resetColor . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsNormal($string) {
        echo PHP_EOL . ' ' . $string . PHP_EOL;
    }

    /**
     * @param $string
     */
    public static function returnAsError($string) {
        echo PHP_EOL . "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        echo "!! ERROR:" . GlobalFunctions::$lightRed . $string . GlobalFunctions::$resetColor . PHP_EOL;
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" . PHP_EOL;
        exit(1);
    }

}

/**
 * Class ParseConfig
 */

class ParseConfig
{

    /**
     * @var string
     */
    public $iniFile = null;


    /**
     * @param string $iniFilePath path to ini file
     */
    public function __construct($iniFilePath) {
        $this->iniFile = $iniFilePath;
    }

    /**
     * @param String $ini Path to Config ini
     * @return array
     * @throws Exception Will be thrown if config.ini can't be accessed.
     */
    function parse_ini($ini) {
        if (file_exists($ini)) {
            $parse = parse_ini_file($ini, true);
            return $parse;
        } else {
            throw new Exception("Config file can't be accessed. Aborting.");
        }
    }

    /**
     * @param String $string Name of the option
     * @param array $parse Handle for the ini file
     * @return mixed
     * @throws Exception Will be thrown if option can't be read.
     */
    function get_option($string, $parse) {
        if (!empty($parse)) {
            $output = $parse[$string];
        } else {
            throw new Exception("Config file can't be accessed.");
        }
        if (isset($output)) {
            return $output;
        } else {
            throw new Exception("Option $string not defined in config.ini.");
        }
    }

    /**
     * @return mixed Path to folder containing local copy of git branch
     */
    public function getPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            if ($GLOBALS['trunk']) {
                $output = $this->get_option("trunk_path", $parse);
            } else {
                $output = $this->get_option("branch_path", $parse);
            }
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }


    /**
     * @return bool Checks config, returns true if form2 tables should be handled
     */
    public function hasForm() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_form2", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }

    /**
     * @return bool Checks config, returns true if qfq tables should be handled
     */
    public function hasQFQ() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("export_qfq", $parse);
            if ($output==1) {
                return true;
            }
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return false;
    }

    /**
     * @return string A temporary output path for the merged files generated by this script
     */
    public function getOutputPath() {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $output = $this->get_option("tmp_path", $parse);
            return $output . "/";
        }
        catch(Exception $e){
            GlobalFunctions::returnAsError($e->getMessage());
        }
        return "";
    }

    /**
     * @param bool $getForm if true, returns the database containing the form2/qfq tables, else returns t3 database
     * @return PDO link to MySQL Database defined in config.ini
     */
    public function getConnection($getForm) {
        $ini = $this->iniFile;
        try {
            $parse = $this->parse_ini($ini);
            $driver = $this->get_option("db_driver", $parse);
            $dsn = "${driver}:";
            $user = $this->get_option("db_user", $parse);
            $password = $this->get_option("db_password", $parse);
        }
        catch (Exception $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }

        $parseTypo = "branch_t3";
        $parseForm = "branch_db";
        if ($GLOBALS["trunk"]) {
            $parseTypo = "trunk_t3";
            $parseForm = "trunk_db";
        }
        if (!empty($parse)) {
            if (!$getForm) {

                foreach ($parse [$parseTypo] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            } else {
                foreach ($parse [$parseForm] as $k => $v) {
                    if (!empty($dsn)) {
                        $dsn .= "${k}=${v};";
                    }
                }
            }
        }
        try {
            if (!empty($dsn) && !empty($user) && !empty($password)) {
                $link = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $link;
            }
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError("Connection failed: " . $e->getMessage());
            exit(1);
        }
        return false;
    }
}

/**
 * Class Results
 */
class Results {
    /**
     * @var array
     */
    public $data_array;
    /**
     * @var string
     */
    public $path;
    /**
     * @var string
     */
    public $mainPath;
    /**
     * @var string
     */
    public $primaryId;
    /**
     * @var string
     */
    public $folder;

    /**
     * @return string
     * @throws Exception
     */
    function encodeJson() {
        if (!empty($this->data_array)) {
            return json_encode($this->data_array, JSON_PRETTY_PRINT);
        } else {
            throw new Exception("Generated JSON was empty. Aborting");
        }
    }

    /**
     * @param ParseConfig $parseConfig
     * @throws Exception
     */
    function createPath($parseConfig) {
        if (isset($this->path)) {
            $mkDir = $this->path;
            if (!file_exists($mkDir)) {
                try {
                    $path = $parseConfig->getPath();
                    if (file_exists($path)) {
                        GlobalFunctions::returnAsList("Path: " . $mkDir . " will be created");
                        mkdir($mkDir);
                    } else {
                        throw new Exception("Configured path can't be accessed: " . $this->path);
                    }
                }
                catch(Exception $e) {
                    GlobalFunctions::returnAsError($e->getMessage());

                }
            }
        } else {
            throw new Exception("Path variable couldn't be retrieved.");
        }
    }

    /**
     * @param $parseConfig
     * @throws Exception
     */
    function createFile( $parseConfig) {
        if(isset($this->path)) {
            try {
                $this->createPath($parseConfig);
                $file = $this->path . '/' . $this->data_array["$this->primaryId"];
                $handle = fopen($file, 'w');
                if ((bool)$handle) {
                    $encodedJ = $this->encodeJson();
                    fwrite($handle, $encodedJ);
                    fclose($handle);
                } else {
                    throw new Exception("File: $file couldn't be opened");
                }
            }
            catch(Exception $e) {
                GlobalFunctions::returnAsError($e->getMessage());
            }
        } else {
            throw new Exception("Path couldn't be set. Probably missing in Config File.");
        }
    }

    /**
     * @param PDO $link
     * @param string $table
     * @param string $class
     * @param ParseConfig $parseConfig
     */
    static function exportData($link, $table, $class, $parseConfig) {
        try {
            /**
             * @var PDOStatement $stmt
             */
            $stmt = $link->prepare("SELECT * FROM " . $table . ";");
            $stmt->execute();
            $path = $parseConfig->getPath();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                /**
                 * @var Results $curPage
                 */
                $curPage = new $class;
                $curPage->data_array = $row;
                $curPage->path = $path . $curPage->folder;
                try {
                $curPage->createFile($parseConfig);
                }
                catch(Exception $e) {
                    GlobalFunctions::returnAsError($e->getMessage());
                }
            }
            $stmt->closeCursor();
        }
        catch(PDOException $e) {
            GlobalFunctions::returnAsError($e->getMessage());
        }
    }
}

/**
 * Class Pages
 */
class Pages extends Results {

    function __construct() {
        $this->folder = "pages";
        $this->primaryId = "uid";
    }
}

/**
 * Class TtContent
 */
class TtContent extends Results {

    function __construct() {
        $this->folder = "tt_content";
        $this->primaryId = "uid";
    }
}

/**
 * Class Form
 */
class Form extends Results {

    function __construct() {
        $this->folder = "form";
        $this->primaryId = "id";
    }
}

/**
 * Class FormElement
 */
class FormElement extends Results {

    function __construct() {
        $this->folder = "form_element";
        $this->primaryId = "id";
    }
}

/**
 * Class QFQForm
 */
class QFQForm extends Results {

    function __construct() {
        $this->folder = "Form";
        $this->primaryId = "id";
    }
}

/**
 * Class QFQFormElement
 */
class QFQFormElement extends Results {

    function __construct() {
        $this->folder = "FormElement";
        $this->primaryId = "id";
    }
}

// Execution Code starts here

// Set ini File containing settings
$loadBaseIni = new ParseConfig($ini);
$pdoTypo= $loadBaseIni->getConnection(false);
$pdoMath= $loadBaseIni->getConnection(true);

GlobalFunctions::returnAsTitle("Exporting MySQL to " . $loadBaseIni->getPath());

Results::exportData($pdoTypo, 'pages', 'Pages', $loadBaseIni);
Results::exportData($pdoTypo, 'tt_content', 'TtContent', $loadBaseIni);

if ($loadBaseIni->hasForm()) {
    Results::exportData($pdoMath, 'form', 'Form', $loadBaseIni);
    Results::exportData($pdoMath, 'form_element', 'FormElement', $loadBaseIni);
}

if ($loadBaseIni->hasQFQ()) {
    Results::exportData($pdoMath, 'Form', 'QFQForm', $loadBaseIni);
    Results::exportData($pdoMath, 'FormElement', 'QFQFormElement', $loadBaseIni);
}

GlobalFunctions::returnAsNormal("Finished export.");
exit(0);