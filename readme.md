Typo3 Deployment
================

Scripts to sync a typo3 productive instance with multiple development instances.

 - Concepts and additional Information: [Project Description](project.md)
 - For information on how to install the t3_deploy enviroment: [Installation](installation.md)

Configuration
-------------

An example config file:

```
branch_path=/tmp/branch/
trunk_path=/tmp/trunk/
tmp_path=/tmp/new/
db_driver=mysql
db_user=deploy
db_password=topSecretPassword
git_repository=git@systemvcs.math.uzh.ch:web/qfqtest.git
git_branch=testing

export_form2=1
export_qfq=0

[branch_t3]
host=localhost
port=3306
dbname=t3_agt2

[branch_db]
host=localhost
port=3306
dbname=math_t2

[trunk_t3]
host=localhost
port=3306
dbname=t3_agt1

[trunk_db]
host=localhost
port=3306
dbname=math_t1

```

Note that there are dsn for t3 and db, since the form / qfq data ussually isn't in the t3 database.

Usage
-----

Run the ./database_sync.sh Script.

 - database_sync.sh -c config/myConfig.ini -o initialize -m "Initializing a trunk"
  - Sets up the Trunk and should be run on productive servers only.

 - database_sync.sh -c config/myConfig.ini -o startBranch -m "Starting my branch called Issue592"
  - Starts your own Branch. 
  - Also imports current Trunk files into the branch 
  - Resets your development instance to the state of the trunk

 - database_sync.sh -c config/myConfig.ini -o synchronize -m "Added a new formular for registering guests"
  - Synchronizes the Branch with the current Trunk 
  - Merges the Branch back to Trunk
  - reimports the merged files into the Branch for testing. 
  
After synchronizing, please check your dev instance first before
importing the new version to the Trunk!