Glossary
--------

Name | Description
-----|------------
Trunk | Trunk is a term used to describe the current stable state of a git repository
Branch | Branches are temporary splits from the Trunk that are usually unstable and under active development.
Merge | Merge is the process of reintegrating a branch to the current state of the Trunk (or another branch).
SQLite | SQLite is a local sql database that is in this project temporary and only exist in memory during the execution time.
Stable | Stable defines a state of the software that was tested and can be used productively
Productive Server | The term Productive Server refers here to a Server that hosts a typo3 website that is or will be available to users.
Development Instance | A typo3 website that is used solely for development. Can be on the productive server or on a designated server. Has its own database and webspace.
t3 | The shorthand t3 refers in this project to typo3.
qfq | A plugin for typo3 that allows you to create dynamic content and forms across your typo3 pages.

Overview
--------

![Overview.svg](https://git.math.uzh.ch/typo3/t3_deployment/uploads/59775b957acf9e6df0c7fd23d4935f8a/Overview.svg)


Merge Handling
--------------

Short | Description
------|-------------
A | Initial checkout of the branch, created by startBranch or last synchronize.
B | The actual state of the development instance inside current develop mysql database.
C | Actual state of the trunk. Should be what the productive server is running.
D | Resulting database after the merge. This will be exported.

Changeset | Description
------|-------------
A-B | Changes of initial state of the branch to current development state
A-C | Changes between the initial checkout of the branch and the trunk.
B-C | Changes between the trunk and the current development database.

### States
Short | Description
------|-------------
1 | Modified (Update), new state differentiates from source.
2 | New (Insert), Entry / id didn't exist previously
3 | Removed (Delete), Entry doesn't exist anymore in the newer state.
4 | No change
5 | Doesn't exist yet on both trunk and branch. 

Possible Changeset combination and what needs to happen to create D.

A-B | A-C | to do in D
----|-----|-----------
1 | 1 | Conflict, user has to fix the conflict or tell git what to do.
1 | 2 | not possible
1 | 3 | Insert B
1 | 4 | Insert B
1 | 5 | not possible
2 | 1 | not possible
2 | 2 | renumber B, keep C
2 | 3 | not possible
2 | 4 | not possible
2 | 5 | insert B
3 | 1 | insert C
3 | 2 | not possible
3 | 3 | delete
3 | 4 | delete C
3 | 5 | not possible
4 | 1 | insert C
4 | 2 | not possible
4 | 3 | delete B
4 | 4 | insert C
4 | 5 | not possible
5 | 1 | not possible
5 | 2 | insert C
5 | 3 | not possible
5 | 4 | not possible
5 | 5 | eternity


 * B-C is regarded with state 5.
 * On Conflict the user has to confirm further actions.
 * On not possible the Program should exit and display an error.
 * Renumber of parent objects has to change child too. Renumber of pages has to regard subpages.