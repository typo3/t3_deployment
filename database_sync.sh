#!/bin/bash

display_help() {
    echo "  Usage:
         database_sync.sh -c [path_to_config.ini] -o [initialize|startBranch|synchronize|resolveConflict]
         -m 'comment'

     path_to_config.ini  Config ini as defined in the readme.md
     initialize          Creating a new Trunk
     startBranch         Creating a Branch from the Trunk in git
     synchronize         Synchronizing and merging Branch to Trunk
     updateBranch        Getting the newest Version of the Branch
     resolveConflict     Resolve the conflict first and then commit again
     updateTrunk         Writes changes to Trunk"
    exit 1
}

COUNT=0;

while getopts c:m:o: params
do
    case $params in
        c)
            INI="$OPTARG"
            ;;
        m)
            MESSAGE="$OPTARG"
            ;;
        o)
            OPTION="$OPTARG"
            ;;
    esac
done

if [[ -z "$INI" || -z "$MESSAGE" || -z "$OPTION" ]]; then
    echo "Not all switches are set, aborting"
    display_help
fi

if [[ -f "$INI" ]]; then
    PATH_PWD="`pwd`"
    PATH_BRANCH=$(awk -F "=" '/branch_path/ {print $2}' ${INI})
    PATH_TRUNK=$(awk -F "=" '/trunk_path/ {print $2}' ${INI})
    PATH_TMP=$(awk -F "=" '/tmp_path/ {print $2}' ${INI})
    GIT_REPO=$(awk -F "=" '/git_repository/ {print $2}' ${INI})
    GIT_BRANCH=$(awk -F "=" '/git_branch/ {print $2}' ${INI})
else
    echo "Ini File is not accesible";
    exit 1;
fi

PHP=php

check_error() {
    local returnCode=$1
    if [ ${returnCode} != 0 ]; then
    exit 1;
    fi
}

check_gitmerge() {
    local returnCode=0
    if [ ${returnCode} != 0 ]; then
    echo "Merge found a conflict, please open the conflicting entry in an editor and look for the git conflict"
    echo "The files are in ${PATH_BRANCH}. Please read above git return for which file is causing the conflict"
    echo 'Afterwards run ./database_sync config/config.ini resolveConflict "Fixing merge Issue xy"'
    echo "last commit:"
    echo "----------------------------------------------------------------------------------------------------"
    git show
    exit 1;
    fi
}
create_folder() {
    local targetFolder=$1

    if [ ! -d "$targetFolder" ]; then
        mkdir "$targetFolder"
    else
        rm -rf "${targetFolder}/*"
    fi
}

clean_folder() {
    local targetFolder=$1

    if [ -d "$targetFolder" ]; then
        rm -rf "${targetFolder}/*"
    else
        echo "Error: Folder " + $1 + " doesn't exist. Requires initialized git repo. Aborting."
        exit 1
    fi
}

initialize_trunk() {

    export_trunk

    cd ${PATH_TRUNK}
    check_error $?

    git init .
    touch .gitignore
    git add --all
    git commit -m "$MESSAGE"

    git remote add origin ${GIT_REPO}

    git push origin --all
    git push origin --tags
}


initialize_from_trunk() {
    create_folder ${PATH_BRANCH}

    cd ${PATH_BRANCH}
    git init .
    git pull ${GIT_REPO}
    git branch ${GIT_BRANCH}
    git checkout ${GIT_BRANCH}
    git commit -a -m "$MESSAGE";
    git push --set-upstream ${GIT_REPO} ${GIT_BRANCH}

    #Setup Trunk folder for later
    setup_trunk_folder

    #Importiere Trunk into branch database
    import_branch
}

setup_trunk_folder() {
    create_folder ${PATH_TRUNK}
    cd ${PATH_TRUNK}
    git init .
    git pull ${GIT_REPO} master
}
export_trunk() {
    create_folder ${PATH_TRUNK}
    cd ${PATH_PWD}
    ${PHP} MysqlToFile.php ${INI} trunk
    check_error $?
}

import_branch() {
    cd ${PATH_PWD}
    ${PHP} FileToMysql.php ${INI}
    check_error $?
}

import_trunk() {
    cd ${PATH_PWD}
    ${PHP} FileToMysql.php ${INI} trunk
}

make_sure_branch_is_checked_out() {
    cd ${PATH_BRANCH}
    git checkout ${GIT_BRANCH}
}
merge_branch_and_push() {
    cd ${PATH_BRANCH}
    git add --all
    remove_deleted_files
    git commit -a -m "${MESSAGE}"
    git push ${GIT_REPO} ${GIT_BRANCH}
    git pull --rebase ${GIT_REPO}
    check_gitmerge $?
    git checkout master
    git pull --rebase ${GIT_REPO}
    git merge ${GIT_BRANCH} -s "recursive" -X "theirs" -m "merging ${MESSAGE}"
    #git commit -a -m "Merging from Branch: ${MESSAGE}"
    git push --set-upstream ${GIT_REPO} master
    check_gitmerge $?
}

remove_deleted_files() {
    cd ${PATH_BRANCH}
    bash -xe ${PATH_TMP}/.deleteScript
    rm -f ${PATH_TMP}/.deleteScript
}

save_old_branch() {
    cd ${PATH_PWD}
    ${PHP} MysqlToFile.php ${INI}
    check_error $?
    cd ${PATH_BRANCH}
    git commit -a -m "Commit before synchronize ran: $MESSAGE"
    git push ${GIT_REPO} ${GIT_BRANCH}
}

resolve_merge_commit() {
    cd ${PATH_BRANCH}
    git commit -a -m "${MESSAGE}"
    git push --set-upstream ${GIT_REPO} master
    check_gitmerge $?
}

pull_trunk_files_from_git() {
    cd ${PATH_TRUNK}
    git pull ${GIT_REPO} master
}

pull_branch_files_from_git() {
    cd ${PATH_BRANCH}
    git pull ${GIT_REPO} ${GIT_BRANCH}
}

get_trunk() {
    if [ -d ${PATH_TRUNK} ]; then
       pull_trunk_files_from_git
    else
       setup_trunk_folder
    fi
}

sync_databases() {
    get_trunk
    make_sure_branch_is_checked_out

    create_folder ${PATH_TMP}
    cd ${PATH_PWD}
    ${PHP} CollisionHandler.php ${INI}
    check_error $?
    move_temp
}

move_temp() {
    # Move temporary files back to branch
    clean_folder ${PATH_BRANCH}
    copy_source="${PATH_TMP}/*"
    cp -r ${copy_source} "${PATH_BRANCH}/."
}

#if [ "$#" -ne 2 ]; then
#    display_help
#fi

case ${OPTION} in
 "initialize")
    echo "Initializing Trunk Repos"
    initialize_trunk
    ;;
 "synchronize")
    echo "Synchronizing Branch and Trunk"
    sync_databases
    #update_trunk no need for that
    merge_branch_and_push
    import_branch
    ;;
 "startBranch")
    echo "Creating branch"
    initialize_from_trunk
    ;;
 "resolveConflict")
    echo "Trying to resolve conflict"
    echo "Please edit files first"
    resolve_merge_commit
    ;;
 "updateBranch")
    echo "Updating Branch"
    pull_branch_files_from_git
    import_branch
    ;;
 "updateTrunk")
    echo "Updating Trunk"
    pull_trunk_files_from_git
    import_trunk
    ;;
 *)
    display_help
    ;;
esac

echo -e "\n \033[0;32mDone.\033[0m\n";